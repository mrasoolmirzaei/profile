package handlers

import (
	"net/http"
	"profile/model"
	"text/template"
)

func Profile(w http.ResponseWriter, r *http.Request) {

	c, err := r.Cookie("session_token")
	if err != nil {
		t, _ := template.ParseFiles("./templates/unauthorized.html")
		t.Execute(w,nil)
		return
	}

	sessionToken := c.Value
	var session model.Session
	db.First(&session, "session_token = ?", sessionToken)

	//userSession, exists := sessions[sessionToken]
	if session.SessionToken == ""{
		t, _ := template.ParseFiles("./templates/unauthorized.html")
		t.Execute(w,nil)
		return
	}
	if session.IsExpired() {
		db.Delete(&session, "session_token = ?", sessionToken)
		t, _ := template.ParseFiles("./templates/unauthorized.html")
		t.Execute(w,nil)
		return
	}

	var user model.User
	db.First(&user, "username = ?", session.Username)
	if user.Username == ""{
		t, _ := template.ParseFiles("./templates/unauthorized.html")
		t.Execute(w,nil)
		return
	}

	t, _ := template.ParseFiles("./templates/profile.html")
	t.Execute(w,user)

}
