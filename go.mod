module profile

go 1.16

require (
	github.com/google/uuid v1.3.0
	gorm.io/driver/mysql v1.3.2
	gorm.io/gorm v1.23.1
)
