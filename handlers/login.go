package handlers

import (
	"github.com/google/uuid"
	"log"
	"net/http"
	"profile/model"
	"text/template"
	"time"
)

type LoginInfo struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

func Login(w http.ResponseWriter, r *http.Request){

	t, _ := template.ParseFiles("./templates/login.html")

	if r.Method != "POST"{
		t.Execute(w, nil)
		return
	}

	if err := r.ParseForm(); err != nil{
		log.Fatal(err)
	}

	loginInfo := LoginInfo{
		Username: r.PostForm.Get("username"),
		Password: r.PostForm.Get("password"),
	}

	var user model.User
	db.Where("username = ?", loginInfo.Username).Where("password = ?", loginInfo.Password).First(&user)
	if user.Username == ""{
		t, _ := template.ParseFiles("./templates/login.html")

		t.Execute(w,struct{ WrongLogin bool }{true})
		return
	}

	sessionToken := uuid.NewString()
	expiresAt := time.Now().Add(60 * time.Second)
	session := model.Session{
		SessionToken: sessionToken,
		Username: user.Username,
		Expiry: expiresAt,
	}

	db.Create(&session)

	http.SetCookie(w, &http.Cookie{
		Name:    "session_token",
		Value:   sessionToken,
		Expires: expiresAt,
	})



	http.Redirect(w, r, "/profile", http.StatusTemporaryRedirect)
}
