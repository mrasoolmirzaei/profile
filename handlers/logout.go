package handlers

import (
	"net/http"
	"profile/model"
	"text/template"
	"time"
)

func Logout(w http.ResponseWriter, r *http.Request) {

	c, err := r.Cookie("session_token")
	if err != nil {
		t, _ := template.ParseFiles("./templates/unauthorized.html")
		t.Execute(w, nil)
		return
	}

	sessionToken := c.Value
	var session model.Session
	db.First(&session, "session_token = ?", sessionToken)

	if session.SessionToken == "" {
		t, _ := template.ParseFiles("./templates/unauthorized.html")
		t.Execute(w, nil)
		return
	}

	db.Delete(&session, "session_token = ?", sessionToken)

	http.SetCookie(w, &http.Cookie{
		Name:    "session_token",
		Value:   "",
		Expires: time.Now(),
	})

	http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
}
