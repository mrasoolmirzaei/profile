package handlers

import (
	"gorm.io/gorm"
	"net/http"
)

var db *gorm.DB

func New(_db *gorm.DB) http.Handler {

	db = _db

	mux := http.NewServeMux()

	//mux.HandleFunc("/home",  Home)

	mux.HandleFunc("/", Login)
	mux.HandleFunc("/profile", Profile)
	mux.HandleFunc("/logout", Logout)
	mux.HandleFunc("/edit", Edit)
	mux.HandleFunc("/signup", Signup)
	//mux.HandleFunc("/auth/google/login", oauthGoogleLogin)
	//mux.HandleFunc("/auth/google/callback", oauthGoogleCallback)

	return mux
}