package handlers

import (
	"net/http"
	"text/template"
)

func Home(w http.ResponseWriter, r *http.Request){
	t, _ := template.ParseFiles("./templates/login.html")

	t.Execute(w,nil)
}
