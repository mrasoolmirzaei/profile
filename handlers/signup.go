package handlers

import (
	"github.com/google/uuid"
	"log"
	"net/http"
	"profile/model"
	"text/template"
	"time"
)

type Credentials struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

func Signup(w http.ResponseWriter, r *http.Request){

	t, _ := template.ParseFiles("./templates/signup.html")

	if r.Method != "POST"{
		t.Execute(w, nil)
		return
	}

	if err := r.ParseForm(); err != nil{
		log.Fatal(err)
	}

	credentials := Credentials{
		Username: r.PostForm.Get("username"),
		Password: r.PostForm.Get("password"),
	}

	var user model.User
	db.Where("username = ?", credentials.Username).Where("password = ?", credentials.Password).First(&user)
	if user.Username != ""{
		t, _ := template.ParseFiles("./templates/signup.html")

		t.Execute(w,struct{ AlreadyExists bool }{true})
		return
	}

	log.Printf("Craeting user...")

	//var user model.User
	db.Create(&model.User{Username: credentials.Username, Password: credentials.Password})

	sessionToken := uuid.NewString()
	expiresAt := time.Now().Add(60 * time.Second)
	session := model.Session{
		SessionToken: sessionToken,
		Username: credentials.Username,
		Expiry: expiresAt,
	}

	db.Create(&session)

	http.SetCookie(w, &http.Cookie{
		Name:    "session_token",
		Value:   sessionToken,
		Expires: expiresAt,
	})

	http.SetCookie(w, &http.Cookie{
		Name:    "register",
		Value:   "incomplete",
		Expires: time.Now().Add(24 * time.Hour),
	})

	http.Redirect(w, r, "/edit", http.StatusTemporaryRedirect)
}
