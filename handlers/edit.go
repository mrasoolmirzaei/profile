package handlers

import (
	"log"
	"net/http"
	"profile/model"
	"text/template"
	"time"
)

var register string

func Edit(w http.ResponseWriter, r *http.Request) {
	c, err := r.Cookie("session_token")
	if err != nil {
		t, _ := template.ParseFiles("./templates/unauthorized.html")
		t.Execute(w,nil)
		return
	}

	_, err2 := r.Cookie("register")
	//if err != nil {
	//	t, _ := template.ParseFiles("./templates/unauthorized.html")
	//	t.Execute(w,nil)
	//	return
	//}



	sessionToken := c.Value
	var session model.Session
	db.First(&session, "session_token = ?", sessionToken)

	//userSession, exists := sessions[sessionToken]
	if session.SessionToken == ""{
		t, _ := template.ParseFiles("./templates/unauthorized.html")
		t.Execute(w,nil)
		return
	}
	if session.IsExpired() {
		db.Delete(&session, "session_token = ?", sessionToken)
		t, _ := template.ParseFiles("./templates/unauthorized.html")
		t.Execute(w,nil)
		return
	}

	var user model.User
	db.First(&user, "username = ?", session.Username)
	if user.Username == ""{
		t, _ := template.ParseFiles("./templates/unauthorized.html")
		t.Execute(w,nil)
		return
	}

	if r.Method != "POST" || err2 == nil{
		http.SetCookie(w, &http.Cookie{
			Name:    "register",
			Value:   "",
			Expires: time.Now(),
		})
		t, _ := template.ParseFiles("./templates/edit.html")
		t.Execute(w,user)
		return
	}

	if err := r.ParseForm(); err != nil{
		log.Fatal(err)
	}

	userUpdated := model.User{
		FullName: r.PostForm.Get("full_name"),
		Address: r.PostForm.Get("address"),
		Email: r.PostForm.Get("email"),
		Telephone: r.PostForm.Get("telephone"),
	}

	db.Model(&user).Updates(userUpdated)

	http.Redirect(w, r, "/profile", http.StatusTemporaryRedirect)
}
