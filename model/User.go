package model

import "gorm.io/gorm"

type User struct {
	gorm.Model
	FullName	string
	Address		string
	Telephone	string
	Email		string
	Username    string
	Password    string
}
