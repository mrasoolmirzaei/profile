package main

import (
	"fmt"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"log"
	"net/http"
	"profile/handlers"
	"profile/model"
)


func main() {
	// refer https://github.com/go-sql-driver/mysql#dsn-data-source-name for details
	dsn := "rasool:0D1MM1FMIQ7ekdlH7uQZ@tcp(127.0.0.1:3306)/db?charset=utf8mb4&parseTime=True&loc=Local"
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		panic("failed to connect database")
	}

	// Migrate the schema
	db.AutoMigrate(&model.User{}, &model.Session{})
	db.Create(&model.User{Username: "rasool", Password: "1", FullName: "mrm", Email: "mrasool.mi",Telephone: "936", Address: "tehran"})
	server := &http.Server{
		Addr: fmt.Sprintf(":8000"),
		Handler: handlers.New(db),
	}

	log.Printf("Starting HTTP Server. Listening at %q", server.Addr)
	if err := server.ListenAndServe(); err != http.ErrServerClosed {
		log.Printf("%v", err)
	} else {
		log.Println("Server closed!")
	}
}