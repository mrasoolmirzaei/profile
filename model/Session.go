package model

import (
	"gorm.io/gorm"
	"time"
)

type Session struct {
	gorm.Model
	SessionToken 	string
	Username 		string
	Expiry   		time.Time
}

func (s Session) IsExpired() bool {
	return s.Expiry.Before(time.Now())
}
